% f=imread('Ex31.tif');
% 读取彩色的lena图像并转为灰度图像
f=imread('lena.jpg');
f=rgb2gray(f);
% f=imread('Ex32.tif');
% Canny图像边缘检测
% 两个阈值
% threshold 敏感度阈值
% 	[low high]， low 和 high 值在 (0 1)
%   或 [0.4*high high]
% sigma 滤波器的标准差 默认值为 sqrt(2)
g1=edge(f,'Canny',0.1);
g2=edge(f,'Canny',0.3);
g3=edge(f,'Canny',0.5);
g4=edge(f,'Canny',0.7);
g5=edge(f,'Canny',0.9);
% 画图
ROW=2;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(g1);title('Canny 0.04,0.1');
subplot(ROW,COL,3);imshow(g2);title('Canny 0.12,0.3');
subplot(ROW,COL,4);imshow(g3);title('Canny 0.20,0.5');
subplot(ROW,COL,5);imshow(g4);title('Canny 0.28,0.7');
subplot(ROW,COL,6);imshow(g5);title('Canny 0.36,0.9');