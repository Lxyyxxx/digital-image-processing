f=imread('Ex5.jpg');
[row,col,rgb]=size(f);
for i=1:rgb
    % 转到0-1
    doublef(:,:,i)=mat2gray(f(:,:,i));
end
% r=doublef(:,:,1);
% g=doublef(:,:,2);
% b=doublef(:,:,3);
% doubleresult=ColorSlicingCube(doublef,[0.6863,0.1608,0.1922],0.2549);
doubleresult=ColorSlicingCube(doublef,[1.0,0.0,0.0],0.73);
r=im2uint8(doubleresult(:,:,1));
g=im2uint8(doubleresult(:,:,2));
b=im2uint8(doubleresult(:,:,3));
for i=1:rgb
    % 转到0-255
    result(:,:,i)=im2uint8(doubleresult(:,:,i));
end
% 画图
ROW=2;COL=3;
subplot(ROW,COL,1);imshow(r);title('红');
subplot(ROW,COL,2);imshow(g);title('绿');
subplot(ROW,COL,3);imshow(b);title('蓝');
subplot(ROW,COL,4);imshow(f);title('原图');
% subplot(ROW,COL,5);imshow(result);title('rgb=[0.6863,0.1608,0.1922],W=0.2549结果');
subplot(ROW,COL,5);imshow(result);title('rgb=[1.0,0.0,0.0],W=0.73结果');