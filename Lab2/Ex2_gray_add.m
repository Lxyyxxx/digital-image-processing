f=imread('lena.jpg');
f=rgb2gray(f);
[row,col]=size(f);
ff=zeros(row,col,8,'uint8');
add_f=zeros(row,col,'double');
for i=1:8
    % 给lena加上加性噪声（高斯噪声）
    ff(:,:,i)=imnoise(f,'gaussian');
    % 图像相加
    add_f=imadd(add_f,double(ff(:,:,i)));
end
% 求平均并转成uint8
add_f=add_f/8.0;
result=im2uint8(mat2gray(add_f));
% 画图
ROW=2;COL=5;
for i=1:8
    subplot(ROW,COL,i);imshow(ff(:,:,i));title(['噪声图',num2str(i)]);
end
subplot(ROW,COL,9);imshow(f);title('原图');
subplot(ROW,COL,10);imshow(result);title('结果');