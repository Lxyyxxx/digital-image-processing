f1=imread('Ex31.tif');
f2=imread('Ex32.tif');
% 缩放至同一大小
f2=imresize(f2,size(f1));
% 图像相加
g1=imadd(f1,f2);
% 图像相减
% g2=imabsdiff(f1,f2);
g2=imsubtract(f1,f2);
% 画图
ROW=1;COL=4;
subplot(ROW,COL,1);imshow(f1);title('图1');
subplot(ROW,COL,2);imshow(f2);title('图2');
subplot(ROW,COL,3);imshow(g1);title('相加');
subplot(ROW,COL,4);imshow(g2);title('相减');