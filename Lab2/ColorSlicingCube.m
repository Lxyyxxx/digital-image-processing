function result = ColorSlicingCube(f,color,W)
%COLORSLIDECUBE 立方体彩色分层
%   f - 原图像
%   color - 立方体中心点颜色，RGB
%   W - 立方体的宽度
[row,col,rgb]=size(f);
logical flag;
W=W/2;
for i=1:row
    for j=1:col
        flag=true;
        for k=1:rgb
            if(abs(f(i,j,k)-color(k))>W)
                flag=false;
                break;
            end
        end
        if flag
            for k=1:rgb
                result(i,j,k)=f(i,j,k);
            end
        else
            for k=1:rgb
                result(i,j,k)=0.5;
            end
        end
    end
end
end

