f=imread('Ex31.tif');
% 二值化
% 方法是将输入图像中亮度大于阈值 T 的所有像素替换为值 1（白色），将所有其他像素替换为值 0（黑色）
bw1=imbinarize(f,0.1);
bw2=imbinarize(f,0.5);
bw3=imbinarize(f,0.9);
% 基本的全局图像阈值
T1=autothresh(f,0.5,0);
bw4=imbinarize(f,T1);
% Otsu全局图像阈值
T2=graythresh(f);
bw5=imbinarize(f,T2);
% 画图
ROW=2;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(bw1);title('0.1');
subplot(ROW,COL,3);imshow(bw2);title('0.5');
subplot(ROW,COL,4);imshow(bw3);title('0.9');
subplot(ROW,COL,5);imshow(bw4);title('基本 0.2772');
subplot(ROW,COL,6);imshow(bw5);title('Otsu 0.2745');