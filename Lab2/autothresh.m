function T = autothresh(f,T,deltaT)
%AUTOTHRESH 基本的全局图像阈值
% input:
%   f - 原图像
%   T - 初始阈值
%   deltaT - 0和1个数差的阈值
% output:
%   T - 最终的阈值
% 将图像转为0-1之间
f=mat2gray(f);
[row,col]=size(f);
% 大于阈值的个数
s1=sum(sum(f>T));
% 小于阈值的个数
s2=row*col-s1;
% 记录之前的阈值
preT=2;
% 迭代，当阈值一直在变且两个值个数差值大于预定义参数
while abs(s1-s2)>deltaT && preT~=T
    % 计算平均灰度值
    m1=sum(sum(f(f>T)))/s1;
    m2=sum(sum(f(f<=T)))/s2;
    preT=T;
    T=(m1+m2)/2;
    % 大于阈值的个数
    s1=sum(sum(f>T));
    % 小于阈值的个数
    s2=row*col-s1;
end
end

