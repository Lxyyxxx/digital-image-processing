function result = ColorSlicingSphere(f,color,R)
%COLORSLICINGSPHERE 球彩色分层
%   f - 原图像
%   color - 球体中心点颜色，RGB
%   R - 球体的半径
[row,col,rgb]=size(f);
double sum;
R=R*R;
for i=1:row
    for j=1:col
        sum=0.0;
        for k=1:rgb
            sum=sum+(f(i,j,k)-color(k))*(f(i,j,k)-color(k));
        end
        if sum>R
            for k=1:rgb
                result(i,j,k)=0.5;
            end
        else
            for k=1:rgb
                result(i,j,k)=f(i,j,k);
            end
        end
    end
end

