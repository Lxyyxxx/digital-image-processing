% f=imread('DSC_0005.tiff');
f=imread('Ex31.tif');
% f=imread('Ex32.tif');
% 高斯滤波
f1=GaussFilter(f,240);
% 拉普拉斯算子滤波
ff=imfilter(f,fspecial('laplacian',0));
ff1=imfilter(f1,fspecial('laplacian',0));
% 与原图相加锐化，并将负值修剪为0
c=-1;
g=uint8(imadd(double(f),c.*double(ff)));
g1=uint8(imadd(double(f),c.*double(ff1)));
% 画图
ROW=2;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(ff);title('拉普拉斯滤波');
subplot(ROW,COL,3);imshow(g);title('拉普拉斯锐化');
subplot(ROW,COL,4);imshow(f1);title('高斯低通滤波');
subplot(ROW,COL,5);imshow(ff1);title('高斯低通滤波+拉普拉斯滤波');
subplot(ROW,COL,6);imshow(g1);title('高斯低通滤波+拉普拉斯锐化');
% imwrite(g,'moon.tiff');

% f=imread('图片1.tiff');
% 二值化
% fbw=im2uint8(mat2gray(imbinarize(f,graythresh(f))));
% 均值滤波
% ffil=imfilter(f,fspecial('average'));

% gbw=imfilter(fbw,fspecial('laplacian'));
% gfil=imfilter(ffil,fspecial('laplacian'));
% [row,col]=size(f);
% 转为double进行运算，防止溢出
% fd=double(f);
% g=zeros(row,col);
% for i=2:row-1
%     for j=2:col-1
%         g(i,j)=fd(i+1,j)+fd(i-1,j)+fd(i,j+1)+fd(i,j-1)-4*fd(i,j);
%     end
% end
% g=im2uint8(mat2gray(abs(g)));
% 画图
% ROW=2;COL=2;
% subplot(ROW,COL,1);imshow(f);title('原图');
% subplot(ROW,COL,2);imshow(fbw);title('原图二值化');
% subplot(ROW,COL,2);imshow(ffil);title('原图均值滤波');
% subplot(ROW,COL,3);imshow(g);title('拉普拉斯锐化');
% subplot(ROW,COL,4);imshow(gbw);title('二值化+拉普拉斯锐化');
% subplot(ROW,COL,4);imshow(gfil);title('均值滤波+拉普拉斯锐化');