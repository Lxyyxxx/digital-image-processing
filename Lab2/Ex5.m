f=imread('Ex5.jpg');
[row,col,rgb]=size(f);
for i=1:rgb
    % 转到0-1
    doublef(:,:,i)=mat2gray(f(:,:,i));
end
% 彩色分割
dc1=ColorSlicingCube(doublef,[0.6863,0.1608,0.1922],0.2549);
dc2=ColorSlicingCube(doublef,[1.0,0.0,0.0],0.73);
ds1=ColorSlicingSphere(doublef,[0.6863,0.1608,0.1922],0.1765);
ds2=ColorSlicingSphere(doublef,[1.0,0.0,0.0],0.5);
for i=1:rgb
    % 转到0-255
    c1(:,:,i)=im2uint8(dc1(:,:,i));
    c2(:,:,i)=im2uint8(dc2(:,:,i));
    s1(:,:,i)=im2uint8(ds1(:,:,i));
    s2(:,:,i)=im2uint8(ds2(:,:,i));
end
% 画图
ROW=2;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(c1);title('rgb=[0.6863,0.1608,0.1922],W=0.2549');
subplot(ROW,COL,3);imshow(c2);title('rgb=[1.0,0.0,0.0],W=0.73');
subplot(ROW,COL,4);imshow(s1);title('rgb=[0.6863,0.1608,0.1922],R=0.1765');
subplot(ROW,COL,5);imshow(s2);title('rgb=[1.0,0.0,0.0],R=0.6');