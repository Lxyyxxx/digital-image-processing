% f=imread('FigP4.5(a)(HeadCT_corrupted).tif');
f=imread('Fig0441(a)(characters_test_pattern).tif');
% 二维快速傅里叶变换
g=fft2(f);
% 中心化
% 第一象限与第三象限交换，第二象限与第四象限交换
gc=fftshift(g);
% 复数转为实数,即取实部的绝对值
g=abs(g);
gc=abs(gc);
% 变换，使最大最小值不会差距太大
g=log(g+1);
gc=log(gc+1);
% 转到0-255
g=im2uint8(mat2gray(g));
gc=im2uint8(mat2gray(gc));
% 画图
ROW=1;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(g);title('傅里叶变换');
subplot(ROW,COL,3);imshow(gc);title('中心化');