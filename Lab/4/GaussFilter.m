function g = GaussFilter(f,d0)
%GAUSSFILTER 高斯低通滤波器
% input:
%   f-原图
%   d0-阶段频率
% output:
%   g-高斯低通滤波后的结果

% 获取图像原始大小
[row,col]=size(f);
% 调整大小
f(2*row,2*col)=0;
% 移至变换中心
for i=1:2*row
    for j=1:2*col
        fp(i,j)=(-1)^(i+j)*f(i,j);
    end
end
% DFT
F=fft2(fp);
% 滤波
for i=1:2*row
    for j=1:2*col
        % 点到频率平面原点的距离
        d=sqrt((i-row)*(i-row)+(j-col)*(j-col));
        % 滤波函数
        H=exp(-d*d/(2*d0*d0));
        % 应用到DFT
        G(i,j)=H*F(i,j);
    end
end
% IDFT，得到处理后的图像
gp=real(ifft2(G));
% 移回去
for i=1:2*row
    for j=1:2*col
        g(i,j)=(-1)^(i+j)*gp(i,j);
    end
end
% 提取左上角区域
g=g(1:row,1:col);
% 转为uint8
g=im2uint8(mat2gray(g));
end