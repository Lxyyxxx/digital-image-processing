f=imread('Fig0441(a)(characters_test_pattern).tif');
% f=imread('FigP4.5(a)(HeadCT_corrupted).tif');
% 高斯低通滤波器
g30=GaussFilter(f,30);
g80=GaussFilter(f,80);
g230=GaussFilter(f,230);
% 画图
ROW=1;COL=4;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(g30);title('d0=30的高斯低通滤波器');
subplot(ROW,COL,3);imshow(g80);title('d0=80的高斯低通滤波器');
subplot(ROW,COL,4);imshow(g230);title('d0=230的高斯低通滤波器');