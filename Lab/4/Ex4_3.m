f=imread('Fig0441(a)(characters_test_pattern).tif');
% f=imread('FigP4.5(a)(HeadCT_corrupted).tif');
% 高斯低通滤波器
g1=GaussFilter(f,230);
% 均值滤波
g2=imfilter(f,fspecial('average',3));
% 画图
ROW=1;COL=3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(g1);title('d0=230的高斯低通滤波器');
subplot(ROW,COL,3);imshow(g2);title('3*3的均值滤波器');