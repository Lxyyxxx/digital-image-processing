% f=imread('Fig1.jpg');
% c=[0.5,3.0,200.5];
% [row,col]=size(c);
% subplot(1,col+1,1);imshow(f);title('原图');
% for i=1:col
%     s=im2uint8(mat2gray(c(i)*log(1.0+double(f))));
%     subplot(1,col+1,i+1);imshow(s);title(['c=',num2str(c(i)),' 对数变换']);
% end

% f=imread('Fig1.jpg');
% c=3;
% s1=im2uint8(mat2gray(c*log2(1.0+double(f))));
% s2=im2uint8(mat2gray(c*log(1.0+double(f))));
% s3=im2uint8(mat2gray(c*log10(1.0+double(f))));
% subplot(1,4,1);imshow(f);title('原图');
% subplot(1,4,2);imshow(s1);title('c=3 log2  对数变换');
% subplot(1,4,3);imshow(s2);title('c=3 ln    对数变换');
% subplot(1,4,4);imshow(s3);title('c=3 log10 对数变换');

% f=imread('Fig1.jpg');
% s1=im2uint8(mat2gray(0.5*log(1.0+double(f))));
% s2=im2uint8(mat2gray(3.0*log(1.0+double(f))));
% s3=im2uint8(mat2gray(90.5*log(1.0+double(f))));
% subplot(1,4,1);imshow(f);title('原图');
% subplot(1,4,2);imshow(s1);title('c=0.5 对数变换');
% subplot(1,4,3);imshow(s2);title('c=3.0 对数变换');
% subplot(1,4,4);imshow(s3);title('c=90.5 对数变换');
% [row,col]=size(f);
% sum(sum(s2==s3))
% row*col

% f=imread('Fig1.jpg');
% b=[1.5,2.0,3.5];
% [row,col]=size(b);
% subplot(1,col+1,1);imshow(f);title('原图');
% for i=1:col
%     s=im2uint8(3.0*mat2gray(f).^b(i));
%     subplot(1,col+1,i+1);imshow(s);title(['c=3.0,b=',num2str(b(i)),' 指数变换']);
% end

% f=imread('Fig1.jpg');
% s1=im2uint8(3.0*mat2gray(f).^1.5);
% s2=im2uint8(3.0*mat2gray(f).^2.0);
% s3=im2uint8(3.0*mat2gray(f).^3.5);
% subplot(1,4,1);imshow(f);title('原图');
% subplot(1,4,2);imshow(s1);title('c=3,b=1.5 指数变换');
% subplot(1,4,3);imshow(s2);title('c=3,b=2.0 指数变换');
% subplot(1,4,4);imshow(s3);title('c=3,b=3.5 指数变换');

f=imread('Fig1.jpg');
c=[0.5,1.5,3.0,5.5];
[row,col]=size(c);
subplot(1,col+1,1);imshow(f);title('原图');
for i=1:col
    s=im2uint8(c(i)*mat2gray(f).^2);
    subplot(1,col+1,i+1);imshow(s);title(['c=',num2str(c(i)),',b=2 指数变换']);
end

% s1=im2uint8(mat2gray(3*power(f,2))); error
% s2=im2uint8(mat2gray(3.0*power(double(f),2.0))); others
% s3=im2uint8(c*mat2gray(f).^b); teacher
