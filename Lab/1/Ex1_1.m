f=imread('Fig1.jpg');
c=3;
b=2;
s1=im2uint8(mat2gray(c*log(1+double(f))));
s2=im2uint8(c*mat2gray(f).^b);
subplot(1,3,1);imshow(f);title('原图');
subplot(1,3,2);imshow(s1);title('c=3对数变换');
subplot(1,3,3);imshow(s2);title('c=3,b=2指数变换');