f = imread('fig0335.tif');
% 中值滤波
f1 = medfilt2(f,[3,3]);
% 均值滤波
h = fspecial('average',3);
f2 = imfilter(f,h);
% 画图
ROW = 1;
COL = 3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(f1);title('3*3中值滤波');
subplot(ROW,COL,3);imshow(f2);title('3*3均值滤波');