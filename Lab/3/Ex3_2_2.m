f = imread('fig0335.tif');
% 不同中值滤波
f1 = medfilt2(f,[3,3]);
f2 = medfilt2(f,[5,5]);
% 画图
ROW = 1;
COL = 3;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(f1);title('3*3中值滤波');
subplot(ROW,COL,3);imshow(f2);title('5*5中值滤波');