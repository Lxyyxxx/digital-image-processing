f = imread('lena.jpg');
% 转为灰度图像
g = rgb2gray(f);
% sobel算子
[Gx, Gy] = imgradientxy(g, 'sobel');
% 这里的Gdir是用atan2算的，范围在-180 ~ 180
% [Gmag, Gdir] = imgradient(Gx, Gy);
% imshow(Gdir);
% Gdir_ = im2uint8(mat2gray(Gdir+180));
% imshow(Gdir_);
% imshowpair(Gmag, Gdir, 'montage')
% title('Gradient Magnitude (Left) and Gradient Direction (Right)');
% G = im2uint8(mat2gray(Gmag));

% 检测竖直边缘
sobel_x = [ -1 0 1; -2 0 2; -1 0 1];
% 检测水平边缘
sobel_y = [ -1 -2 -1; 0 0 0; 1 2 1];
% 均值滤波
% Gx = imfilter(g, sobel_x);
% Gy = imfilter(g, sobel_y);
% Gx = filter2(fspecial('sobel')', g);
% Gy = filter2(fspecial('sobel'), g);
% Gx = filter2(sobel_x, g);
% Gy = filter2(sobel_y, g);

% 梯度幅值图像
G = im2uint8(mat2gray(sqrt(double(Gx).^2 + double(Gy).^2)));

% 梯度方向图像
% -90 ~ 90 -> 0 ~ 180
% theta = atan(Gy./Gx) * 180./pi; 
% [row, col] = size(theta);
% for i=1:row
%     for j=1:col
%         if Gx(i,j) > 0 && Gy(i,j) >= 0
%             Gtheta(i,j) = theta(i,j);
%         elseif Gx(i,j) < 0 && Gy(i,j) >= 0
%             Gtheta(i,j) = 180 + theta(i,j);
%         elseif Gx(i,j) < 0 && Gy(i,j) < 0
%             Gtheta(i,j) = 180 + theta(i,j);
%         elseif Gx(i,j) > 0 && Gy(i,j) < 0
%             Gtheta(i,j) = 360 + theta(i,j);
%         elseif Gx(i,j) == 0 && Gy(i,j) >= 0
%             Gtheta(i,j) = 90;
%         elseif Gx(i,j) == 0 && Gy(i,j) < 0
%             Gtheta(i,j) = 270;
%         end
%     end
% end
% Gc = im2uint8(mat2gray(Gtheta));

% -2/pi ~ 2/pi -> 0 ~ 2pi
theta = atan(Gy./Gx);
[row, col] = size(theta);
for i=1:row
    for j=1:col
        if Gx(i,j) > 0 && Gy(i,j) >= 0
            Gtheta(i,j) = theta(i,j);
        elseif Gx(i,j) < 0 && Gy(i,j) >= 0
            Gtheta(i,j) = pi + theta(i,j);
        elseif Gx(i,j) < 0 && Gy(i,j) < 0
            Gtheta(i,j) = pi + theta(i,j);
        elseif Gx(i,j) > 0 && Gy(i,j) < 0
            Gtheta(i,j) = 2*pi + theta(i,j);
        elseif Gx(i,j) == 0 && Gy(i,j) >= 0
            Gtheta(i,j) = pi/2;
        elseif Gx(i,j) == 0 && Gy(i,j) < 0
            Gtheta(i,j) = pi*3/2;
        end
    end
end
Gp = im2uint8(mat2gray(Gtheta));

% 画图
ROW = 2;
COL = 4;
subplot(ROW,COL,1);imshow(f);title('原图');
subplot(ROW,COL,2);imshow(g);title('灰度图像');
subplot(ROW,COL,3);imshow(im2uint8(mat2gray(Gx)));title('0-1 再 0-255 的 Gx');
subplot(ROW,COL,4);imshow(im2uint8(mat2gray(Gy)));title('0-1 再 0-255 的 Gy');
subplot(ROW,COL,5);imshow(uint8(Gx));title('直接舍去 <0 和 >255 的 Gx');
subplot(ROW,COL,6);imshow(uint8(Gy));title('直接舍去 <0 和 >255 的 Gy');
subplot(ROW,COL,7);imshow(G);title('梯度幅值图像');
subplot(ROW,COL,8);imshow(Gp);title('梯度方向图像');