数字图像处理
===

# 1 灰度变换
![1-1](pics/1/1-1.png)
![1-2](pics/1/1-2.png)
![1-3](pics/1/1-3.png)
![1-4](pics/1/1-4.png)
![1-5](pics/1/1-5.png)

# 2 直方图均衡
![1-1](pics/2/1-1.png)

# 3 图像的卷积和滤波
![1-1](pics/3/1-1.png)
![1-2](pics/3/1-2.png)
![1-3](pics/3/1-3.png)
![1-4](pics/3/1-4.png)
![1-5](pics/3/1-5.png)

# 4 傅立叶变换和频域滤波
![1-1](pics/4/1-1.png)
![1-2](pics/4/1-2.png)
![1-3](pics/4/1-3.png)
![1-4](pics/4/1-4.png)
![1-5](pics/4/1-5.png)
