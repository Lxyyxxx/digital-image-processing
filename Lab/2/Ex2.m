grayPic = imread('Fig0308(a)(pollen).tif');
% 得到直方图
picHist = imhist(grayPic);
% 第一次直方图均衡化
newPic = histeq(grayPic);
% 第二次直方图均衡化
pic2 = histeq(newPic);
% 子图大小
ROW = 3; COL = 2;
subplot(ROW,COL,1);imshow(grayPic);title('原图像');
subplot(ROW,COL,2);imhist(grayPic);title('原直方图');
subplot(ROW,COL,3);imshow(newPic);title('第一次直方图均衡化后的图像');
subplot(ROW,COL,4);imhist(newPic);title('第一次直方图均衡化后的直方图');
subplot(ROW,COL,5);imshow(pic2);title('第二次直方图均衡化后的图像');
subplot(ROW,COL,6);imhist(pic2);title('第二次直方图均衡化后的直方图');